### BUILD
To build the mandelbrot program, run the following in a terminal:
```Rust
cargo build --release

time /target/release/mandelbrot 4000x3000 -1.20,0.35 -1,0.20 --threaded
```
The above example runs the program concurrently, if you wish to see the compile time single threaded, change `--threaded` to `--single`
