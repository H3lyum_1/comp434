### BUILD
To build, clone repository and cd into project. Assuming you have Rust's package manager `Cargo` installed, run project using
```Rust  
cargo run
```
Navigate to `localhost:9000` and will be greeted with a simple webpage. Every new request you maket to the site will be executed by a new thread. These threads can be seen in the terminal screen.
