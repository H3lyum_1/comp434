### BUILD
To build both the client and server, open a terminal window for both server and client and execute the following:

```Rust
cargo build

cargo run
```
To create new clients, open another terminal window and execute same commands above. The server window will display that a new client is connected.

To exit the client from the session, type `:q` in the window and the client will be disconnected.
