### Projects
The following three project utilise the Rust package manager `Cargo`. To run these programs you will need to have it installed.

To install, follow the instructions at `https://doc.rust-lang.org/cargo/getting-started/installation.html`.

Once installed, cd in the project and follow the README for each on how to build and run.
